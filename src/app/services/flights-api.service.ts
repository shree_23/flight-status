import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import moment from 'moment';
import { Constants } from '../constants/constants';
import { UtilsService } from '../services/utils.service';
import { Flights, OperationalFlights } from '../interfaces/model';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FlightsApiService {

  constructor(public http: HttpClient, public consts: Constants, public utils: UtilsService) { }

  getFlightStatusAPI(requestParams: string | object, serializeRequired?: boolean): Observable<Flights | OperationalFlights> {
    /*
      service call which request flight status api with defined params
    */
    const queryParams = serializeRequired ? this.utils.serializeRequestParams(requestParams) : requestParams;
    const url = this.consts.FLIGHT_STATUS_API_URL + '' + queryParams;
    const headers = this.consts.HEADERS;

    return this.http.get<OperationalFlights | Flights>(url, headers).pipe(map(res => res ));
  }

  processFlightResponse(flights): Array<Flights> {
      /*
        flight searched by flight, origin and destination
       */
    return this.utils.processFlightDataFromResponse(flights);
  }

}

