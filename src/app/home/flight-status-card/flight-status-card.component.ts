import { Component, OnInit, Input } from '@angular/core';
import { Flights } from '../../interfaces/model';

@Component({
  selector: 'app-flight-status-card',
  templateUrl: './flight-status-card.component.html',
  styleUrls: ['./flight-status-card.component.scss']
})
export class FlightStatusCardComponent implements OnInit {

  flight: Array<Flights>;
  constructor() { }

  @Input() flightResponse: Array<Flights>;

  ngOnInit() {
    this.flight = this.flightResponse;
  }

}
