import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Constants } from '../constants/constants';
import { UtilsService } from '../services/utils.service';
import { ObservableData, Flights } from '../interfaces/model';
import { FlightsApiService } from '../services/flights-api.service';
import { OperationalFlights } from '../interfaces/model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit, OnDestroy {

  flights: Array<Flights>;
  showFlightStatusError = false;
  showLoader: boolean;
  observableObject = null;
  subscriptionObject = null;
  threshold = 0;
  begin = 0;
  end = this.threshold;
  spil = 0;
  currentTime: string;
  tomorrow: string;

  constructor(
    public flightService: FlightsApiService,
    public consts: Constants, public utils: UtilsService,
    public flightServiceApi: FlightsApiService,
    private changeDetection: ChangeDetectorRef) { }

  ngOnInit() {
    /**
     * subscribe to flight data publisher, handle it once recieved
     */
    this.currentTime = this.utils.getDateByFormat('Do MMM h:mm a', 0);
    this.tomorrow = this.utils.getDateByFormat('Do MMM 23:59 a', 1);

    this.threshold = this.consts.lazyLoadChunkThreshold;
    this.end = this.threshold;
    this.showLoader = true;
    /*
     * load all flights by default with current time until tomorrow
     */
    this.loadInitialAllFlightS();

    this.observableObject = this.utils.getData().subscribe((data: ObservableData) => {
      if (data) {
        if ((data.data.showLoader || data.data.hideLoader)) {
          this.showLoader = data.data.showLoader || data.data.hideLoader;
        } else {
          this.handleFlightResponse(data);
        }
      }
    });

    const $ele = document.querySelector('.main-container');
    $ele.addEventListener('scroll', (e: Event): void => {
      // tslint:disable-next-line:no-string-literal
      if (Math.round(e.target['scrollTop']) === (e.target['scrollHeight'] - e.target['offsetHeight'])) {

        this.lazyLoadFlightResponseInChunk(this.flightService.processFlightResponse(this.utils.getFlightsData())).forEach(flight => {
          if (this.flights.indexOf(flight) === -1) {
            this.flights.push(flight);
          }
        });
        this.changeDetection.detectChanges();

        // tslint:disable-next-line:no-string-literal
      } else if (Math.round(e.target['scrollTop']) === 0) {

        this.resetLazyLoadingSettings();
        this.flights = this.lazyLoadFlightResponseInChunk(this.flightService.processFlightResponse(this.utils.getFlightsData()));
        this.changeDetection.detectChanges();
      }
    }, false);
  }

  loadInitialAllFlightS(): void {
    const reqParam = {
      startRange: this.consts.DEFAULT_START_TIME,
      endRange: this.consts.DEFAULT_END_TIME
    };
    this.subscriptionObject = this.flightServiceApi.getFlightStatusAPI(reqParam, true).subscribe((flightResponse: OperationalFlights) => {
      this.handleFlightResponse({ data: flightResponse, err: null });
    }, err => {
      this.showFlightStatusError = true;
    });
  }
  resetLazyLoadingSettings(): void {
    this.threshold = this.consts.lazyLoadChunkThreshold;
    this.begin = 0;
    this.end = this.threshold;
    this.spil = 0;
  }
  handleFlightResponse(data): void {
    /*
     * once flights data receieved, this funchtion process the data and display into view
     */
    this.flights = null;
    this.resetLazyLoadingSettings();

    if (data && !data.data.err && (!data.data.showLoader && !data.data.hideLoader)) {

      const operationalFlights =
        data.data.flightResponse ? data.data.flightResponse.operationalFlights :
          data.data.operationalFlights;

      this.utils.setFlightsData(operationalFlights);

      this.flights =
        this.lazyLoadFlightResponseInChunk(
          this.flightService.processFlightResponse(operationalFlights));
      this.showLoader = false;
      /*
       * change detection strategy is set to on push
       */
      setTimeout(() => {
        /**
         * scrolling the element to top once view is ready
         */
        const $ele = document.querySelector('.main-container');
        const offsetHeight = 'offsetHeight';
        $ele.scrollTop = document.querySelector('.flight-search-container.top-section')[offsetHeight];
      }, 10);
    } else if (data && data.data.err) {
      /*
        show error if anything fails
       */
      this.showFlightStatusError = true;
      this.flights = null;
    }
    this.showLoader = false;
    this.changeDetection.detectChanges();
  }

  lazyLoadFlightResponseInChunk(flights: Array<Flights>): Array<Flights> {
    /**
     * lazy flights data part by part from flights data cahce
     */
    let flightsChunk;
    if (flights.length > this.threshold) {
      this.spil = Math.ceil(flights.length % this.threshold);
      if (this.spil === (flights.length - this.begin)) {
        flightsChunk = flights.slice(this.begin);
      } else {
        flightsChunk = flights.slice(this.begin, this.end);
      }
      this.begin += this.threshold;
      this.end = this.begin * 2;
    }
    return flightsChunk || flights;
  }

  ngOnDestroy() {
    if (this.observableObject) {
      this.observableObject.unsubscribe();
    }
    if (this.subscriptionObject) {
      this.subscriptionObject.unsubscribe();
    }
  }
}
